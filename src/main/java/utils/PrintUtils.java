package utils;

import java.util.stream.Collectors;

import org.protelis.lang.datatype.DeviceUID;
import org.protelis.lang.datatype.Field;

public class PrintUtils {
	
	private PrintUtils() { /** Private constructor to hide public one */ }

	public static String fieldToString(Field<Object> field) {
		return field.toMap().toString();
	}
	
	public static String fieldIdsToString(Field<?> field) {
		return field.keyStream().map(DeviceUID::toString)
			.collect(Collectors.joining(", "));
	}
	
	public static void console(Object obj) { System.out.println(obj); }
}
