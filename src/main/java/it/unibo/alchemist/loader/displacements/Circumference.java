/*
 * Copyright (C) 2010-2019, Danilo Pianini and contributors listed in the main project's alchemist/build.gradle file.
 *
 * This file is part of Alchemist, and is distributed under the terms of the
 * GNU General Public License, with a linking exception,
 * as described in the file LICENSE in the Alchemist distribution's top directory.
 */
/**
 * 
 */
package it.unibo.alchemist.loader.displacements;

import static org.apache.commons.math3.util.FastMath.PI;
import static org.apache.commons.math3.util.FastMath.cos;
import static org.apache.commons.math3.util.FastMath.sin;
import static org.apache.commons.math3.util.FastMath.sqrt;

import org.apache.commons.math3.random.RandomGenerator;

import it.unibo.alchemist.model.interfaces.Environment;
import it.unibo.alchemist.model.interfaces.Position;

/**
 * @param <P>
 */
public final class Circumference<P extends Position<? extends P>> extends AbstractRandomDisplacement<P> {

    private final double centerx, centery, radius;
    private final int nodeCount;

    /**
     * @param pm
     *            the {@link Environment}
     * @param rand
     *            the {@link RandomGenerator}
     * @param nodes
     *            the number of nodes
     * @param centerX
     *            the center x of the circle
     * @param centerY
     *            the center y of the circle
     * @param radius
     *            the radius of the circle
     */
    public Circumference(final Environment<?, P> pm,
            final RandomGenerator rand,
            final int nodes,
            final double centerX, final double centerY, final double radius) {
		super(pm, rand, nodes);
        this.nodeCount = nodes;
        this.centerx = centerX;
        this.centery = centerY;
        this.radius = radius;
    }

    @Override
    protected P indexToPosition(final int i) {
        final double angle = i * 2 * PI / this.nodeCount;
        return makePosition(centerx + radius * cos(angle), centery + radius * sin(angle));
    }

}
