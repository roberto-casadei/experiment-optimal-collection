package it.unibo.alchemist.custom;

//import org.apache.commons.math3.random.RandomGenerator;

public class PacketLosser {
    private final double R50, R99, K;

    public PacketLosser(double r50, double r99) {
        R50 = r50;
        R99 = r99;
        K = Math.log(6792093./29701) / (R99-R50);
    }

    public boolean succeed(double distance, double r) {
        return r*r*r > 1/(7 * Math.exp(K*(R50-distance)) + 1);
    }
}
