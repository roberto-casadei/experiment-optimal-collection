incarnation: protelis

variables:
  random: &random
    min: 0
    max: 9
    step: 1
    default: 0
  dtime: &dtime
    type: ArbitraryVariable
    parameters: [0.03, [0.01, 0.03, 0.1, 0.3]]
  retain: &retain
    min: 0
    max: 1
    step: 1
    default: 0
  speed: &speed
    formula: 25
  hops: &hops
    formula: 10
  neigh: &neigh
    formula: 25
  rate: &rate               # period length
    formula: 1
  dspace: &dspace           # maximum distance of target (disabled)
    formula: 10 * speed
  vibr: &vibr
    formula: speed / 6
  radius: &radius           # connection radius
    formula: 100
  area: &area               # radius of circular overall area
    formula: hops * 50
  narea: &narea
    formula: -area
  num: &num                 # total number of devices
    formula: neigh * hops * hops / 4
  border: &border           # number of devices on the border to ensures network connection (disabled)
    formula: hops * 4
  middle: &middle           # number of devices in the middle 
    formula: num - 2
  pretain: &pretain
    formula: "(retain == 1) ? 2 : Double.NaN"
  gAlgorithm: &gAlgorithm
    type: ArbitraryVariable
    parameters: [2, [0, 1, 2]] # 0=ABF, 1=FLEX, 2=BIS

export:
 - time
 - molecule: idemp-g-dist_err
   aggregators: [mean]
   value-filter: onlyfinite
 - molecule: arith-g-dist_err
   aggregators: [mean]
   value-filter: onlyfinite

 - molecule: idemp-g-dist_inf
   aggregators: [mean]
 - molecule: arith-g-dist_inf
   aggregators: [mean]

 - molecule: ideal-min
   aggregators: [min]
 - molecule: sp-min
   aggregators: [min]
 - molecule: mp-min
   aggregators: [min]
 - molecule: wmp-min
   aggregators: [min]
 - molecule: list-min
   aggregators: [min]

 - molecule: sp-min_err
   aggregators: [min]
 - molecule: mp-min_err
   aggregators: [min]
 - molecule: wmp-min_err
   aggregators: [min]
 - molecule: list-min_err
   aggregators: [min]

 - molecule: ideal-sum
   aggregators: [sum]
 - molecule: sp-sum
   aggregators: [sum]
 - molecule: mp-sum
   aggregators: [sum]
 - molecule: wmp-sum
   aggregators: [sum]
 - molecule: list-sum
   aggregators: [sum]
 - molecule: listf-sum
   aggregators: [sum]
   
 - molecule: list_avg_sp-sum
   aggregators: [sum]
 - molecule: listf_avg_sp-sum
   aggregators: [sum]
 - molecule: list_avg_mp-sum
   aggregators: [sum]
 - molecule: listf_avg_mp-sum
   aggregators: [sum]

 - molecule: sp-sum_err
   aggregators: [sum]
 - molecule: mp-sum_err
   aggregators: [sum]
 - molecule: wmp-sum_err
   aggregators: [sum]
 - molecule: list-sum_err
   aggregators: [sum]
 - molecule: listf-sum_err
   aggregators: [sum]
   
 - molecule: list_avg_sp-sum_err
   aggregators: [sum]
 - molecule: listf_avg_sp-sum_err
   aggregators: [sum]
 - molecule: list_avg_mp-sum_err
   aggregators: [sum]
 - molecule: listf_avg_mp-sum_err
   aggregators: [sum]

seeds:
  scenario: *random
  simulation: *random

network-model:
  type: ConnectWithinDistance
  parameters: [*radius]

pools:
  - pool: &program
    - time-distribution:
        type: WeibullDistributedWeibullTime
        parameters: [*rate, *dtime, *dtime]
      type: Event
      actions:
        - type: RunProtelisProgram
          parameters: [isolation_test, *pretain]
    - program: send
  - pool: &vibrate
    - time-distribution: 1
      type: Event
      actions:
        - type: BrownianMove
          parameters: [*vibr]
  - pool: &move
    - time-distribution: 5
      type: Event
      actions:
        - type: MoveToTarget
          parameters: [target, *speed]
  - pool: &deviceContent
    - molecule: dspace
      concentration: *dspace
    - molecule: dtime
      concentration: *dtime
    - molecule: speed
      concentration: *speed
    - molecule: rate
      concentration: *rate
    - molecule: radius
      concentration: *radius
    - molecule: area
      concentration: *area
    - molecule: num
      concentration: *num
    - molecule: gAlgorithm
      concentration: *gAlgorithm

displacements:
  - in:                                         # source 1
      type: Point
      parameters: [*narea, 0]
    programs:
      - *program
    contents: *deviceContent
  - in:                                         # source 2
      type: Point
      parameters: [*area,  0]
    programs:
      - *program
    contents: *deviceContent
  - in:                                         # devices
      type: Circle
      parameters: [*middle, 0, 0, *area]
    programs: 
      - *program
      - *move
    contents: *deviceContent
