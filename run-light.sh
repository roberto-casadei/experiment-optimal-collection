#!/bin/bash
set -e

./gradlew le_default &
./gradlew le_speed &
./gradlew le_hops &
./gradlew le_neigh &
./gradlew le_dtime &
