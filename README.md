# Optimal Resilient Distributed Data Collection in Mobile Edge Environments

Submitted to the Elsevier Journal on Computers & Electrical Engineering.
For any issues with reproducing the experiments, please contact [Roberto Casadei](mailto:roby.casadei@gmail.com).

## Overview

This repository contains everything related to the experimental evaluation part of the paper.
It includes:

1. source code of the algorithms, see `src/main/protelis/optimal.pt`
2. source code of the experiments, see `src/main/protelis/isolation_test.pt` (program run by the devices)
 and`src/main/yaml/isolation_test.yml` (descriptor of the Alchemist simulation)
3. instructions for reproducing the experiments (see next)

## Prerequisites

* Importing the repository
    - Git

* Executing the simulations
    - Java 11 JDK installed and set as default virtual machine

* Code inspection and GUI execution
    - Eclipse Neon or Oxygen (older versions should work as well)

* Plotting
    - Python 2.7 and Asymptote

**Notes about the supported operating systems**

All the tests have been performed under Mac OS and Ubuntu Linux. No test has been performed under any other operating system, however, due to the portable nature of the used tools, the test suite should run on any Unix-like OSs.


## Reproducing the experiments

### 1. Importing the repository

The first step is cloning this repository. It can be easily done by using `git`. Open a terminal, move it into an empty folder of your choice, then issue the following command:

```
git clone https://bitbucket.org/roberto-casadei/experiment-optimal-collection.git

```

### 2. Smoke test

This should make all the required files appear in your repository, with the latest version.

To check if everything works as expect, run the following command to start a GUI-based simulation (it may take some time to bootstrap):

```
./gradlew interactive
```

Once the GUI shows up, press **P** to start the simulation.


### 3. Open the project with an IDE

Open Eclipse, click on "File > Import" and then on "Gradle > Existing Gradle Project", then select the folder of the repository donwloaded in the previous step.

To properly visualize the source files you should also install a Yaml editor (as `YEdit` in the Eclipse Marketplace) and the Protelis Parser (also in the Eclipse Marketplace).

### 4. Project layout and contents

The files with the source code are in `src/main/protelis/` and contain the following:

* `utils.pt`: general utility functions
* `distance.pt`: distance estimation algorithm
* `collection.pt`: state-of-the-art collection algorithms
* `optimal.pt`: the new collection algorithms proposed
* `isolation_test.pt`: isolation tests

The files describing the environment are in `src/main/yaml/` and contain the following:

* `isolation_test.yaml`: environment used for the isolation tests

In the `java` folder, there are files improving on the current Alchemist version (scheduled for future releases).
In the `resources` folder, there are effect files for the GUI presentation.
In order to run the simulations with a GUI, create a Run Configuration in Eclipse with the following settings.

* Main class: `it.unibo.alchemist.Alchemist`
* Program arguments: `-g src/main/resources/isolation_test.aes -y src/main/yaml/isolation_test.yml`

### 5. Reproducing the experimental data

Actually, there is no need to use a GUI to run the experiments, as we have provided a Gradle task for that.
Just run:

```
./gradlew isolation_test
```

for running the batch of experiments (NOTE: it requires a lot of resources and might take a lot of time).
Data produced by the simulator will be writted to directory `data/raw/`
(the output is one CSV file per simulation run).

### 6. Processing data and plotting

In order to create plots from the data files, you can use the script `tools/plot_builder.py` toagether with Asymptote, e.g., as follows:

```bash
ddir=../data/raw

python2.7 plot_builder.py $ddir/isolation_test* 'time(sum)' > test_default_sum.asy
python2.7 plot_builder.py $ddir/isolation_test* 'time(min)' > test_default_min.asy
asy *.asy # this will generate a set of pdf for the different plots
```

## High-resolution plots

### Idempotent (MIN) case study

![](images/test_default_min.png)
![](images/test_speed_min_err.png)
![](images/test_hops_min_err.png)
![](images/test_neigh_min_err.png)
![](images/test-timemin-legenda.png)

### Arithmetic (COUNT) case study

![](images/test_default_sum.png)
![](images/test_speed_sum_err.png)
![](images/test_hops_sum_err.png)
![](images/test_neigh_sum_err.png)
![](images/test-timesum-legenda.png)