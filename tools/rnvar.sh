#!/bin/bash
set -e

sed -i 's/list-avg/blist/g' *.asy
sed -i 's/list avg mp/blist/g' *.asy
sed -i 's/listf avg mp/blist \(filtered\)/g' *.asy
sed -i 's/listf/list \(filtered\)/g' *.asy
sed -i 's/(sum err)//g' *.asy
sed -i 's/(sum)//g' *.asy
sed -i 's/(min)//g' *.asy
