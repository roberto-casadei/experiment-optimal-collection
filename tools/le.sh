#!/bin/bash
set -e

ddir=../analysis

python2.7 plot_builder.py $ddir/test_default_* 'time(sum)' > test_default_sum.asy
python2.7 plot_builder.py $ddir/test_default_* 'time(min)' > test_default_min.asy
python2.7 plot_builder.py $ddir/test_default_* 'time(sum_err)' > test_default_sum_err.asy
python2.7 plot_builder.py $ddir/test_default_* 'time(min_err)' > test_default_min_err.asy
python2.7 plot_builder.py $ddir/test_speed*_* 'speed(sum_err)' > test_speed_sum_err.asy
python2.7 plot_builder.py $ddir/test_speed*_* 'speed(min_err)' > test_speed_min_err.asy
python2.7 plot_builder.py $ddir/test_hops*_* 'hops(sum_err)' > test_hops_sum_err.asy
python2.7 plot_builder.py $ddir/test_hops*_* 'hops(min_err)' > test_hops_min_err.asy
python2.7 plot_builder.py $ddir/test_neigh*_* 'neigh(sum_err)' > test_neigh_sum_err.asy
python2.7 plot_builder.py $ddir/test_neigh*_* 'neigh(min_err)' > test_neigh_min_err.asy
python2.7 plot_builder.py $ddir/test_dtime*_* 'dtime(sum_err)' > test_dtime_sum_err.asy
python2.7 plot_builder.py $ddir/test_dtime*_* 'dtime(min_err)' > test_dtime_min_err.asy

# python2.7 plot_builder.py $ddir/le_default* 'retain:time(sum_err)' > ne_default_sum.asy
# python2.7 plot_builder.py $ddir/le_speed_* 'retain,speed:time(sum_err)' > ne_speed_sum.asy
# python2.7 plot_builder.py $ddir/le_hops_*  'retain,hops:time(sum_err)'  > ne_hops_sum.asy
# python2.7 plot_builder.py $ddir/le_neigh_* 'retain,neigh:time(sum_err)' > ne_neigh_sum.asy
# python2.7 plot_builder.py $ddir/le_dtime_* 'retain,dtime:time(sum_err)' > ne_dtime_sum.asy

# python2.7 plot_builder.py $ddir/le_default* 'retain:time(min_err)' > ne_default_min.asy
# python2.7 plot_builder.py $ddir/le_speed_* 'retain,speed:time(min_err)' > ne_speed_min.asy
# python2.7 plot_builder.py $ddir/le_hops_*  'retain,hops:time(min_err)'  > ne_hops_min.asy
# python2.7 plot_builder.py $ddir/le_neigh_* 'retain,neigh:time(min_err)' > ne_neigh_min.asy
# python2.7 plot_builder.py $ddir/le_dtime_* 'retain,dtime:time(min_err)' > ne_dtime_min.asy

# ,mp-sum*retain
#python2.7 plot_builder.py $ddir/le_default_* 'time(list-sum*retain,listf-sum*retain,wmp-sum*retain)' > retain_default_sum.asy
#python2.7 plot_builder.py $ddir/le_speed*_* 'speed:time(list-sum*retain,listf-sum*retain,wmp-sum*retain)' > retain_speed_sum.asy
#python2.7 plot_builder.py $ddir/le_hops*_* 'hops:time(list-sum*retain,listf-sum*retain,wmp-sum*retain)' > retain_hops_sum.asy
#python2.7 plot_builder.py $ddir/le_neigh*_* 'neigh:time(list-sum*retain,listf-sum*retain,wmp-sum*retain)' > retain_neigh_sum.asy
#python2.7 plot_builder.py $ddir/le_dtime*_* 'dtime:time(list-sum*retain,listf-sum*retain,wmp-sum*retain)' > retain_dtime_sum.asy


# defaults: speed: 20.0, hops: 10, neigh: 20, dtime: 0.1

# python2.7 plot_builder.py ../analysis-optc-by-g/goptc* 'gAlgorithm:time(sp-sum,mp-sum,wmp-sum,list-sum,list avg sp-sum,list avg mp-sum,ideal-sum)' > optcg.asy
# python2.7 plot_builder.py ../analysis-optc-by-g/goptc* 'gAlgorithm:time(idemp-g-dist_err,arith-g-dist_err)' > goptc.asy
