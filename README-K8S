1. Install `kubectl` (more info [here](https://kubernetes.io/docs/tasks/tools/install-kubectl/))
    1. Download the latest release with the command: 
    ```bash
    curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
    ```

    2. Make the kubectl binary executable:
    ```bash
    chmod +x ./kubectl
    ```

    3. Move the binary in to your `PATH`:
    ```bash
    sudo mv ./kubectl /usr/local/bin/kubectl
    ```

    4. Install auto-completion
    ```bash
    echo 'source <(kubectl completion bash)' >>~/.bashrc # use .zshrc in case of ZSH
    source ~/.bashrc # use .zshrc in case of ZSH
    ```

2. Configure `kubectl`
    1. Run the autoconfig script for our Area 4.0 Cluster
    ```bash
    sh kube-config-gen.sh $USERNAME $TOKEN
    ```
    where `$USERNAME` and `$TOKEN` must be provided by some K8s admin

    2. Ensure configuration works by running:
    ```bash
    kubectl cluster-info
    ```
    which should output something like:
    ```
    Kubernetes master is running at https://137.204.107.11:6443
    KubeDNS is running at https://137.204.107.11:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
    ```

3. _\[Optional\]_ Enable `kubectl`'s reverse proxy to use the K8s dashboard
    1. On a fresh new shell:
    ```bash
    kubectl proxy
    ```

    2. Use your favourite browser to browse to:
    > http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/

4. You can submit your computational task by running:
    ```bash
    kube-launch-task.sh $TASK_NAME $IMAGE $COMMAND $WORKING_DIRECTORY $SLEEP_TIME
    ```
    where:
    - `TASK_NAME` is a custom task name for your task
    - `IMAGE` is the name of the Docker image your container should be bootstrapped from
    - `COMMAND` is the command to be launched on your container
    - `WORKING_DIRECTORY` is the working directory `COMMAN` will be launched in (defaults to `/`)
    - `SLEEP_TIME` is time the container will wait before terminating, after `COMMAND` has successfully terminated (defaults to 7 days)



